import React, { Component } from 'react';
import { Link } from 'gatsby';
import '../sass/listing.scss';

export default class Listing extends Component {
	render() {
		return (
			<main>
				<ListItem slug='t-shirt' name='T-shirt' description='T-shirt qualité premium, brodé à la main, personnalisé sur demande.' color='primary'/>
				<ListItem slug='sweater' name='Sweat-shirt' description='Sweat-shirt qualité premium, brodé à la main, personnalisé sur demande.' color='secondary'/>
				<ListItem slug='tote-bag' name='Tote bag' description='Tote bag, brodé à la main, personnalisé sur demande.' color='dark'/>
				<ListItem slug='embroidery' name='Broderie décorative' description='Broderie sur canevas, modèle sur demande.' color= 'other'/>
				</main>
		)
	}
}

class ListItem extends Component {
	render() {
		return (
			<Link to={`/items/${this.props.slug}`}>
				<article className={`list-item ${this.props.color}`}>
					<h2>{this.props.name}</h2>
					<p>{this.props.description}</p>
				</article>
			</Link>
		)
	}
}