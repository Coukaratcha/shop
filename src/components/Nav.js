import React, { Component } from 'react';
import { Link } from 'gatsby';
import '../sass/nav.scss';

export default class Nav extends Component {
	render() {
		return (
			<nav>
				<Link className='nav-item underline' to='/'>Accueil</Link>
				<Link className='nav-item underline' to='/items/t-shirt'>T-shirt</Link>
				<Link className='nav-item underline' to='/items/sweater'>Sweat-shirt</Link>
				<Link className='nav-item underline' to='/items/tote-bag'>Tote Bag</Link>
				<Link className='nav-item underline' to='/items/embroidery'>Broderie</Link>
			</nav>
		)
	}
}