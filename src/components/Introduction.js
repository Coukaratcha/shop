import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import '../sass/introduction.scss';



const Introduction = () => {
	const data = useStaticQuery(graphql`
	query {
		site {
			siteMetadata {
				title
				description
			}
		}
	}
	`)
	
	return (
		<section id="introduction">
			<div className='content'>
				<h1>{data.site.siteMetadata.title}</h1>
				<p className='subtitle'>{data.site.siteMetadata.description}</p>
			</div>
		</section>
	)
}

export default Introduction;