import React from 'react';
import { useStaticQuery, graphql } from 'gatsby';
import { useLocation } from '@reach/router';
import { Helmet } from 'react-helmet';
import Header from './Header';

export default function Layout({ pageTitle, children }) {
	const data = useStaticQuery(graphql`
	query {
		site {
			siteMetadata {
				title
				siteUrl
				description
				image
				twitterUsername
			}
		}
	}`);

	const { pathname } = useLocation();

	const seo = {
		title: data.site.siteMetadata.title,
		description: data.site.siteMetadata.description,
		url: `${data.site.siteMetadata.siteUrl}${pathname}`,
		image: data.site.siteMetadata.image,
		twitterUsername: data.site.siteMetadata.twitterUsername
	}

	return (
		<div className='application'>
			<Header />
			<Helmet>
				<meta charSet='utf-8' />
				<title>{pageTitle} | {seo.title}</title>
				<meta name='description' content={seo.description} />
				<meta property='og:url' content={seo.url} />
				<meta property='og:title' content={seo.title} />
				<meta property='og:description' content={seo.description} />
				<meta property='og:image' content={seo.image} />

				<meta name='twitter:card' content='summary_large_image' />
				<meta name='twitter:creator' content={seo.twitterUsername} />
				<meta name='twitter:title' content={seo.title} />
				<meta name='twitter:description' content={seo.description} />
				<meta name='twitter:image' content={seo.image}/>

				<link rel='canonical' href={data.site.siteMetadata.siteUrl}/>
				<link rel='me' href='https://pouet.mickaelgillot.xyz/@Couka' />
			</Helmet>
			{children}
		</div>
	)
}