import React, { Component } from 'react';
import '../sass/footer.scss';

export default class Footer extends Component {
	render() {
		return (
			<footer>
				<h1>Un projet&nbsp;? Une question&nbsp;?</h1>
				<p>
					Envoyez un mail à <a className='underline' href='mailto:contact@mickaelgillot.xyz?subject=[Coukarastitch] Général'>contact@mickaelgillot.xyz</a>
				</p>
			</footer>
		)
	}
}