import React, { Component } from 'react';
import Layout from '../components/Layout';
import Introduction from '../components/Introduction';
import Listing from '../components/Listing';
import Footer from '../components/Footer';

import '../sass/index.scss';

export default class HomePage extends Component {
	render() {
		return (
			<div>
				<Layout pageTitle='Home'>
					<Introduction />
					<Listing />
					<Footer />
				</Layout>
			</div>
		)
	}
}