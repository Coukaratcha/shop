import React, { Component } from 'react';
import { graphql } from 'gatsby';
import { MDXRenderer } from 'gatsby-plugin-mdx';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';
import Layout from '../../components/Layout';
import '../../sass/item.scss';

export const query = graphql`
	query MyQuery($id: String) {
		mdx(id: {eq: $id}) {
			frontmatter {
				title
				price
				eta
			}
			slug
			body
		}

		images: allFile(filter: {extension: {regex: "/(png)|(jpg)|(jpeg)/"}}) {
			edges {
				node {
					id
					base
					relativeDirectory
					childImageSharp {
						gatsbyImageData(
							placeholder: BLURRED
							formats: [AUTO, WEBP, AVIF]
						)
					}
				}
			}
		}
	}
`

export default class ItemPage extends Component {
	render() {

		return (
			<Layout pageTitle={this.props.data.mdx.frontmatter.title}>
				<div id={this.props.data.mdx.slug} className='container'>
					<main>
						<div className='content'>
							<h1>{this.props.data.mdx.frontmatter.title}</h1>
							<MDXRenderer>
								{this.props.data.mdx.body}
							</MDXRenderer>
							<h2>Prix indicatif : {this.props.data.mdx.frontmatter.price}</h2>
							<h2>Délai indicatif : {this.props.data.mdx.frontmatter.eta}</h2>
							<a href={`mailto:contact@mickaelgillot.xyz?subject=[Coukarastitch] ${this.props.data.mdx.frontmatter.title}`} className='list-item-link'>Me contacter</a>
						</div>
					</main>
					<aside>
					{this.props.data.images.edges.filter(img => img.node.relativeDirectory === this.props.data.mdx.slug).map(img => (
						<div key={img.node.id} className='img-container'>
							<GatsbyImage 
								image={getImage(img.node.childImageSharp)}
								alt={img.node.base}
							/>
						</div>
					))}
					<div className='img-container'></div>
					</aside>
				</div>
			</Layout>
		)
	}
}