import React, { Component } from 'react';
import Layout from '../components/Layout';

export default class NotFoundPage extends Component {
	render() {
		return (
			<Layout pageTitle='Not found'>
			404 - Not found
			</Layout>
		)
	}
}