FROM node:alpine as build
WORKDIR /app
RUN apk add --update python3 make g++ && rm -rf /var/cache/apk/*
ENV PATH /app/node_modules/.bin:$PATH
COPY ./package.json ./
COPY ./yarn.lock ./
RUN yarn --silent
COPY . ./
RUN yarn build

FROM nginx:alpine
COPY --from=build /app/public /usr/share/nginx/html/
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]