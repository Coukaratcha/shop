module.exports = {
  siteMetadata: {
    title: `Coukarastitch`,
    siteUrl: `https://shop.mickaelgillot.xyz`,
		description: "Boutique d'objets brodés à la main sur commande. T-shirts, Sweat-shirts, Tote bags, Patchs, Broderie décorative, etc.",
		image: '/triforce-oot.jpg',
		twitterUsername: '@MickaelGillot'
  },
  plugins: [
		"gatsby-plugin-react-helmet",
		"gatsby-plugin-image",
    "gatsby-plugin-sharp",
		"gatsby-transformer-sharp",
		{
			resolve: "gatsby-source-filesystem",
			options: {
				name: `items`,
				path: `${__dirname}/items`
			}
		},
		{
			resolve: "gatsby-source-filesystem",
			options: {
				name: `images`,
				path: `${__dirname}/src/images/`
			}
		},
    "gatsby-plugin-mdx",
		"gatsby-plugin-sass",
	]
};